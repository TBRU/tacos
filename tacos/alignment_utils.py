"""
    tacos.alignment_utils

    Library for reading or re-formatting a genomic alignment table.

    :copyright: (c) 2019 by Monica R. Ticlla
    :license: MIT, see LICENSE for more details.

    This module provides multiple functions for reading a genomic alignment table of the following format:
        +-----------+--------+-----------+----------+-----------+-----+
        |Position   | Gene   |Reference  |GENOME_1  |GENOME_2   | ... |
        +-----------+--------+-----------+----------+-----------+-----+
    Where:
        - Position: int
            Corresponds to a genomic position
        - Gene: string
            Name of Gene
        - Reference: character
            The allele for the corresponding position in the reference genome
        - GENOME_1: character
            The allele for the corresponding position in genome GENOME_1

        We assume alleles are coded following IUPAC's nucleotide convention.

    Available functions:
    - import_alignment: Reads a genomic alignment table into a numeric table.
"""
import numpy as np
import csv

def import_alignment(data_file, sep='\t'):
    """Reads a genomic alignment table into a numeric table.

    This function assigns numeric codes to each nucleotide (including IUPAC's ambiguous nucleotides) and returns a
    positions-by-genomes table. Numeric codes are assigned as in the following dictionary:
        {'A':1, 'G':2, 'C':3, 'T':4, ['Y','R','W','S','K','M','D','V','H','B','X','N']:-1, and ['.','-']:0}

    Args:
        data_file: str
            A valid string path to a genomic alignment table.
        sep: str, default '\t'
            Delimiter to use.
    Returns:
         An m by n array of m original genomic sequences (i.e observations) in an n-dimensional space (genomic positions)
         and two lists. The first list maps the row indices of the numpy.ndarray to the corresponding genomic positions.
         The second list maps the column indices of the numpy.ndarray to the corresponding genomes. In the array,
         allele Values (e.g. 'A', 'T', 'C', 'G') were converted to numerical values (e.g. 1,2,3,4).

    """
    # IUPAC's ambiguous nucleotides
    nucleotides_letters = ['A', 'G', 'C', 'T', 'Y', 'R', 'W', 'S', 'K', 'M', 'D', 'V', 'H', 'B', 'X', 'N', '.', '-']
    # Numeric codes for each nucleotide
    # Ambiguous nucleotides are coded as missing values and receive a -1 value
    # nucleotide_numeric = list(range(1,15)) + [-1,-1,0,0]
    nucleotide_numeric = [1, 2, 3, 4] + [-1] * 12 + [0, 0]
    # Dictionary to map nucleotides letters to corresponding numeric equivalents
    iupac_nucleotide_code_mapper = dict(zip(nucleotides_letters, nucleotide_numeric))

    records_header = list()
    # positions_gene = dict()
    # positions_ref_allele = dict()
    positions = []
    positions_genomes_alleles = []
    alignment_table_reader = csv.reader(open(data_file), delimiter=sep)

    for ix, row in enumerate(alignment_table_reader):
        if (ix == 0):
            records_header = row
        else:
            position = row[0]
            # positions_gene[position] = row[1]
            # positions_ref_allele[position] = row[2]
            positions.append(position)
            alleles, inv_ix = np.unique(row[3:], return_inverse=True)
            alleles_numeric = np.array([iupac_nucleotide_code_mapper[key] for key in alleles], dtype='int8')
            positions_genomes_alleles.append(alleles_numeric[inv_ix])
    genomes = records_header[3:]
    positions_genomes_alleles = np.asarray(positions_genomes_alleles, dtype='int8')
    return (genomes, positions, positions_genomes_alleles.T)


def as_snp_incidence(data_file, sep='\t', ignore_gap=False):
    """Reads a genomic alignment table into a SNP-Genome incidence matrix

    This function reads an alignment table and converts it to a SNP-Genome incidence matrix,
    where rows correspond to SNPs and columns correspond to genomes. Values in each cell are
    either 0 or 1.

    Args:
        data_file: str
            A valid string path to a genomic alignment table. This table has the following format:
            +-----------+--------+-----------+----------+-----------+-----+
            |Position   | Gene   |Reference  |GENOME_1  |GENOME_2   | ... |
            +-----------+--------+-----------+----------+-----------+-----+
            Where:
            - Position: int
                Corresponds to a genomic position
            - Gene: string
                Name of Gene
            - Reference: character
                The allele for the corresponding position in the reference genome
            - GENOME_1: character
                The allele for the corresponding position in genome GENOME_1

            We assume alleles are coded following IUPAC's nucleotide convention.
        sep: str, default '\t'
            Delimiter to use.
        ignore_gap: bool, default False
            By default gaps (i.e '.', '-') are considered a 5th character. If set to True, gaps are encoded as
            missing values and therefore ignored or not considered as an allele.
    Returns: numpy.ndarray
         A SNP by Genome incidence matrix and two lists. The first list maps the row indices of the numpy.ndarray
         to the corresponding SNP names. SNP names have this format S_<position>_<reference allele>_<alternative allele>
         (e.g S_63_A_G). The second list maps the column indices of the numpy.ndarray to the corresponding genome names.
    """
    # IUPAC's anbiguous nucleotides are ignored
    default_characters_to_ignore = ['Y', 'R', 'W', 'S', 'K', 'M', 'D', 'V', 'H', 'B', 'X', 'N']
    # IUPAC's characters for gaps/deletions
    gap_characters = ['.', '-']

    if ignore_gap:
        default_characters_to_ignore.extend(gap_characters)

    genomes = []
    snps = list()
    snps_genomes = list()

    try:
        with open(data_file) as my_file:
            alignment_table_reader = csv.reader(my_file, delimiter=sep)
            for ix, row in enumerate(alignment_table_reader):
                if (ix == 0):
                    genomes = row[3:]
                else:
                    position = row[0]
                    ref_allele = row[2]
                    alt_alleles = np.array(row[3:])
                    to_ignore = np.in1d(alt_alleles, default_characters_to_ignore + [ref_allele])
                    uniq_alt_alleles = np.unique(alt_alleles[~to_ignore])
                    if uniq_alt_alleles.size > 0:
                        for allele in uniq_alt_alleles:
                            snps.append('S_{}_{}_{}'.format(position, ref_allele, allele))
                            snps_genomes.append(np.array(alt_alleles == allele, dtype=np.ubyte))
        return (snps, genomes, np.asarray(snps_genomes, dtype=np.ubyte))
    except OSError as e:
        print('e')
