"""
    tacos.distance

    Library for computing genomic pairwise distance as nucleotide substitutions.

    :copyright: (c) 2019 by Monica R. Ticlla
    :license: MIT, see LICENSE for more details.

    This module provides multiple functions to ...

    Available functions:
    - ...

"""
import numpy as np
from scipy.spatial.distance import pdist, squareform
import itertools
import multiprocessing as mp
from multiprocessing import get_context
from contextlib import closing
from functools import partial
from joblib import Parallel, delayed
import ctypes
import tqdm

def genetic_hamming(seq1, seq2, missing='X', gap='-', ignore_gap=False, adjust=False):
    """Return the Hamming distance between two given sequence of characters or two discrete numerical vectors.
    Sequences must have equal length.

    This function computes the genetic distance between two sequences of nucleotides represented as characters (e.g A,
    T, C, G) or as discrete numerical values (e.g 1, 2, 3, 4)

    Args:
        seq1: numpy.ndarray (faster), pandas.Series
            Sequence of characters or discrete numerical vectors.
        seq1: numpy.ndarray (faster), pandas.Series
            Sequence of characters or discrete numerical vectors.
        missing: Character or value (int) representing missing value in the sequence.
        gap: Character or value (int) representing gap/deletion value in the sequence.
        ignore_gap: boolean, default False
            Flag to consider gaps as missing values instead of coding them as a 5th character
        adjust: boolean, default False
            Flag specifying adjustment of distance to account for missing values.
    Returns:
        Int, Float
        Absolute sequence difference between seq1 and seq2, after excluding positions with missing values.

    """
    # Raises exception if vectors do not have equal length
    try:
        # get positions excluding missing values
        if ignore_gap:
            seq1_keep = ~(np.in1d(seq1,[missing,gap]))
            seq2_keep = ~(np.in1d(seq2,[missing,gap]))
            #seq1_keep = (seq1 != missing) & (seq1 != gap)
            #seq2_keep = (seq2 != missing) & (seq2 != gap)
        else:
            seq1_keep = (seq1 != missing)
            seq2_keep = (seq2 != missing)
        to_keep = (seq1_keep & seq2_keep)
    except ValueError:
        print("seq1 and seq2 must have equal lengths.")
    else:
        # drop positions with missing values and
        # compute Hamming distance
        # np.sum() instead of sum() improves speed considerably!
        new_seq1 = seq1[to_keep]
        new_seq2 = seq2[to_keep]
        pair_dist = np.sum(new_seq1 != new_seq2)
        # Adjust distance to account for missing values
        if adjust:
            seq1_effective_len = np.sum(seq1_keep)
            seq2_effective_len = np.sum(seq2_keep)
            pair_dist = np.around(len(seq1) * (2 * (pair_dist) / (seq1_effective_len + seq2_effective_len)), decimals=2)
        #else:
            #pair_dist = np.sum(new_seq1 != new_seq2)

        return (pair_dist)

def hamming_pairs(genome_pairs, data_array, missing='X', gap='-', ignore_gap=False, adjust=False):
    """
    Args:
        data_array: numpy.ndarray
            An m by n array of m original genomic sequences (i.e observations) in an n-dimensional space (genomic positions).
            Values can either be characters ('A', 'T', 'C', 'G') or numerical values (1,2,3,4). It's strongly recommended
            to provide a ndarray of numerical vectors.
    """
    try:
        pair_distances = np.asarray([genetic_hamming(data_array[g1,:], data_array[g2,:],
                                                     missing, gap, ignore_gap, adjust)
                                     for g1,g2 in tqdm.tqdm(genome_pairs)])
    except IndexError:
        print("Provided index is out of range in data_array.")
    else:
        return(pair_distances)

def hamming_pdist(data_array, missing='X', gap='-', ignore_gap=False, adjust=False, form=1):
    """Computes pairwise genetic Hamming distance between every pair of genomic sequences (i.e observations) in
    n-dimensional space (genomic positions).

    This function uses Scipy's pdist function to apply tacos.distance.genetic_hamming to every pair of genomic sequences in
    data_array. It runs fast but it's not using multiprocessing.

    Args:
        data_array: numpy.ndarray
            An m by n array of m original genomic sequences (i.e observations) in an n-dimensional space (genomic positions).
            Values can either be characters ('A', 'T', 'C', 'G') or numerical values (1,2,3,4). It's strongly recommended
            to provide a numpy.ndarray of numerical vectors.
        missing: str, int, default 'X'
            Value representing missing value in the genomic sequence.
        gap: str, int, default '-'
            Value representing gap/deletion value in the sequence.
        ignore_gap: boolean, default False
            Flag to consider gaps as missing values instead of coding them as a 5th character
        adjust: boolean, default False
            Flag specifying adjustment of genetic Hamming distance to account for missing values.
        form:int, default 1
            Specify the format of the resulting distance matrix as either condensed (1, a one-dimensional vector) or
            square-form distance matrix (2)

    Returns:
        numpy.ndarray
        A condensed distance matrix (vector-form) or a redundant square-form distance matrix.

    """
    try:
        out_dist = pdist(data_array, genetic_hamming, missing=missing, gap=gap, ignore_gap=ignore_gap, adjust=adjust)
    except ValueError as e:
        print(e)
    else:
        if form != 1:
            return(squareform(out_dist))
        else:
            return(out_dist)

def _hamming_pool_init_worker(data_array):
    global data_array_in_worker
    data_array_in_worker = data_array

def _hamming_pool_helper(genome_pairs, missing='X', gap='-', ignore_gap=False, adjust=False):
    try:
        pair_distances = np.asarray([genetic_hamming(data_array_in_worker[g1, :],
                                                     data_array_in_worker[g2, :],
                                                     missing, gap, ignore_gap, adjust)
                                     for g1, g2 in tqdm.tqdm(genome_pairs)])
    except IndexError:
        print("Provided index is out of range in data_array.")
    else:
        return (pair_distances)

def hamming_pool(data_array, missing='X', gap='-', ignore_gap=False, adjust=False, form=1, n_jobs=1):
    """Computes pairwise genetic Hamming distance between every pair of genomic sequences (i.e observations) in
    n-dimensional space (genomic positions).

    This function uses multiprocessing.Pool to apply tacos.distance.genetic_hamming to every pair of genomic sequences in
    data_array.

    Args:
        data_array: numpy.ndarray
            An m by n array of m original genomic sequences (i.e observations) in an n-dimensional space (genomic positions).
            Values can either be characters ('A', 'T', 'C', 'G') or numerical values (1,2,3,4). It's strongly recommended
            to provide a numpy.ndarray of numerical vectors.
        missing: str, int, default 'X'
            Value representing missing value in the genomic sequence.
        gap: str, int, default '-'
            Value representing gap/deletion value in the sequence.
        ignore_gap: boolean, default False
            Flag to consider gaps as missing values instead of coding them as a 5th character
        adjust: boolean, default False
            Flag specifying adjustment of genetic Hamming distance to account for missing values.
        form:int, default 1
            Specify the format of the resulting distance matrix as either condensed (1, a one-dimensional vector) or
            square-form distance matrix (2).
        n_jobs: int
            The number of concurrent processes.

    Returns:
        numpy.ndarray
        A condensed distance matrix (vector-form) or a redundant square-form distance matrix.
    """
    if n_jobs==1:
        return(hamming_pdist(data_array=data_array, missing=missing, gap=gap, ignore_gap=ignore_gap, adjust=adjust,
                             form=form))
    else:
        nr_genomes = data_array.shape[0]
        genome_ix_pairs = list(itertools.combinations(range(0,nr_genomes),2))

        # Here we copy the data_array to a shared memory array
        # Using the shared memory unlock array gained 5 fold more speed
        data_array_shm = mp.Array(ctypes.c_int8, data_array.size, lock=False)
        global data_array_shm_b
        data_array_shm_b = np.frombuffer(data_array_shm, dtype=ctypes.c_int8)
        data_array_shm_b.shape = data_array.shape
        data_array_shm_b[:] = data_array

        # Start a multiprocessing pool
        genome_pairs_chunks = np.array_split(genome_ix_pairs, n_jobs)
        #with closing(get_context("spawn").Pool(processes=n_jobs)) as pool:
        with closing(get_context("spawn").Pool(processes=n_jobs,
                                               initializer=_hamming_pool_init_worker,
                                               initargs=(data_array_shm_b,))) as pool:
            print('starting pool with {} processes'.format(n_jobs))
            #get_distance_partial = partial(hamming_pairs,
            #                               data_array=data_array_shm_b,
            #                               missing=missing, gap=gap, ignore_gap=ignore_gap, adjust=adjust)
            get_distance_partial = partial(_hamming_pool_helper,
                                           missing=missing, gap=gap, ignore_gap=ignore_gap, adjust=adjust)

            distance_mp = pool.imap(get_distance_partial, tqdm.tqdm(genome_pairs_chunks), chunksize=1)
        pool.close()
        pool.join()

        pair_distances = [pair_dist for item in distance_mp for pair_dist in item]
        if form == 1:
            return(pair_distances)
        else:
            return (squareform(pair_distances))

def hamming_joblib(data_array, missing='X', gap='-', ignore_gap=False, adjust=False, form=1, n_jobs=2):
    """Computes pairwise genetic Hamming distance between every pair of genomic sequences (i.e observations) in
    n-dimensional space (genomic positions).

    This function uses joblib.Parallel to apply tacos.distance.genetic_hamming to every pair of genomic sequences in
    data_array.

    Args:
        data_array: numpy.ndarray
            An m by n array of m original genomic sequences (i.e observations) in an n-dimensional space (genomic positions).
            Values can either be characters ('A', 'T', 'C', 'G') or numerical values (1,2,3,4). It's strongly recommended
            to provide a numpy.ndarray of numerical vectors.
        missing: str, int, default 'X'
            Value representing missing value in the genomic sequence.
        gap: str, int, default '-'
            Value representing gap/deletion value in the sequence.
        ignore_gap: boolean, default False
            Flag to consider gaps as missing values instead of coding them as a 5th character
        adjust: boolean, default False
            Flag specifying adjustment of genetic Hamming distance to account for missing values.
        form:int, default 1
            Specify the format of the resulting distance matrix as either condensed (1, a one-dimensional vector) or
            square-form distance matrix (2).
        n_jobs: int
            The number of parallel jobs.

    Returns:
        numpy.ndarray
        A condensed distance matrix (vector-form) or a redundant square-form distance matrix.

    """
    try:
        nr_genomes = data_array.shape[0]
        genome_ix_pairs = list(itertools.combinations(range(0, nr_genomes), 2))
        out_dist = np.asarray(Parallel(n_jobs=n_jobs, prefer='threads')(delayed(genetic_hamming)(data_array[g1_ix, :],
                                                                                                  data_array[g2_ix,:],
                                                                                                 missing = missing,
                                                                                                 gap = gap,
                                                                                                 ignore_gap=ignore_gap,
                                                                                                 adjust=adjust)
                                                                        for g1_ix, g2_ix in tqdm.tqdm(genome_ix_pairs)))
    except ValueError as e:
        print(e)
    else:
        if form != 1:
            return(squareform(out_dist))
        else:
            return(out_dist)