import click
import numpy as np
import itertools
from tacos.alignment_utils import import_alignment
from tacos.alignment_utils import as_snp_incidence
from tacos.distance import hamming_pool

@click.group()
def tacos():
    pass

@tacos.command()
@click.option('--ignore_gap', 'ignore_gap', is_flag=True, help='enable coding gaps(i.e indels) as missing values')
@click.option('-d','--separator', 'sep', default='\t', help='field separator [default: TAB]')
@click.argument('INPUT_', required=True, type=click.Path(exists=True))
@click.argument('OUTPUT', required=True, type=click.Path())
def snp_incidence_matrix(ignore_gap, sep, input_, output):
    """Reads a genomic alignment table into a SNP-Genome incidence matrix.

    \b
    This function reads an alignment table and converts it to a SNP-Genome incidence matrix,
    where rows correspond to SNPs and columns correspond to genomes.
    Values in each cell are either 0 or 1.

    \b
    INPUT is the path to the file of a genomic alignment table. This table has the
    following format:
        +-----------+--------+-----------+----------+-----------+-----+
        |Position   | Gene   |Reference  |GENOME_1  |GENOME_2   | ... |
        +-----------+--------+-----------+----------+-----------+-----+
    \b
    Where:
        - Position: int
            Corresponds to a genomic position
        - Gene: string
            Name of Gene
        - Reference: str
            The allele for the corresponding position in the reference genome.
            Values are any of IUPAC's encoding:
                ['A', 'G', 'C', 'T', 'Y', 'R', 'W', 'S', 'K', 'M', 'D', 'V', 'H', 'B', 'X', 'N', '.', '-']
        - GENOME_1: str
            The allele for the corresponding position in genome GENOME_1
            Values are any of IUPAC's encoding:
                ['A', 'G', 'C', 'T', 'Y', 'R', 'W', 'S', 'K', 'M', 'D', 'V', 'H', 'B', 'X', 'N', '.', '-']
        We assume alleles are coded following IUPAC's nucleotide convention.
    """
    INPUT_file = input_
    snps, genomes, incidence_matrix = as_snp_incidence(INPUT_file, sep=sep, ignore_gap=ignore_gap)
    nr_snps = len(snps)
    with click.open_file(output, 'w') as f:
        f.write('\t'.join(['']+genomes))
        f.write('\n')
        for row_ix in range(nr_snps):
            f.write(snps[row_ix]+'\t')
            f.write('\t'.join(map(str,incidence_matrix[row_ix,:])))
            f.write('\n')
    click.echo('Finished! Created  {} by {} SNP-Genome incidence matrix.'.format(nr_snps, len(genomes)))

@tacos.command()
@click.option('--ignore_gap', 'ignore_gap', is_flag=True, help='enable coding gaps(i.e indels) as missing values')
@click.option('--adjust_na', 'adjust', is_flag=True, help='enable adjustment of distance for missing values')
@click.option('--n_jobs', 'n_jobs', default=1, show_default=True, help='specify number of processes for parallel computing')
@click.option('--type', default='0', show_default=True, type=click.Choice(['0','1','2']),
              help='either 1 (writes the lower triangle of the matrix), or 0 (writes the upper triangle) or 2 (writes both parts)')
@click.argument('INPUT_', required=True, type=click.Path(exists=True))
@click.argument('OUTPUT', required=True, type=click.Path())
def distance_matrix(ignore_gap, adjust, n_jobs, type, input_, output):
    '''Computes the genetic distance between each genome pair in INPUT.

    \b
    Distances are measured as the number of substitutions needed to convert genome g_j to
    genome g_i (i.e Hamming distance). For each pair positions with missing values are ignored.

    \b
    INPUT is the path to the file with variable positions (rows) and genomes (columns) of
    the following format:
        +-----------+--------+-----------+----------+-----------+-----+
        |Position   | Gene   |Reference  |GENOME_1  |GENOME_2   | ... |
        +-----------+--------+-----------+----------+-----------+-----+
    \b
    Where:
        - Position: int
            Corresponds to a genomic position
        - Gene: string
            Name of Gene
        - Reference: str
            The allele for the corresponding position in the reference genome.
            Values are any of IUPAC's encoding:
                ['A', 'G', 'C', 'T', 'Y', 'R', 'W', 'S', 'K', 'M', 'D', 'V', 'H', 'B', 'X', 'N', '.', '-']
        - GENOME_1: str
            The allele for the corresponding position in genome GENOME_1
            Values are any of IUPAC's encoding:
                ['A', 'G', 'C', 'T', 'Y', 'R', 'W', 'S', 'K', 'M', 'D', 'V', 'H', 'B', 'X', 'N', '.', '-']
        We assume alleles are coded following IUPAC's nucleotide convention.
    \b
    Ambiguous nucleotides ('Y', 'R', 'W', 'S', 'K', 'M', 'D', 'V', 'H', 'B', 'X', 'N')
    are encoded as missing values.
    '''
    INPUT_file = input_
    genomes, positions, genomes_positions = import_alignment(INPUT_file, sep='\t')
    nr_genomes = len(genomes)
    genome_pairs = list(itertools.combinations(genomes, 2))

    distance_matrix = hamming_pool(data_array=genomes_positions,
                                   missing=-1,
                                   gap=0,
                                   ignore_gap=ignore_gap,
                                   adjust=adjust,
                                   form=2,
                                   n_jobs=n_jobs)
    if type == '0':
        distance_matrix = np.triu(distance_matrix)
    elif type == '1':
        distance_matrix = np.tril(distance_matrix)
    else:
        pass

    with click.open_file(output, 'w') as f:
        f.write('\t'.join(['']+genomes))
        f.write('\n')
        for row_ix in range(nr_genomes):
            f.write(genomes[row_ix]+'\t')
            f.write('\t'.join(map(str,distance_matrix[row_ix,:])))
            f.write('\n')

    click.echo('Finished! Computed {} pairwise distances for {} genomes.'.format(len(genome_pairs), nr_genomes))

#if __name__ == '__main__':
#    tacos()