from setuptools import setup, find_packages

setup(
    name='tacos',
    author='Monica R. Ticlla',
    author_email='mticlla@gmail.com',
    version='0.2',
    packages=find_packages(include=['tacos']),
    install_requires=[
        'Click',
        'numpy',
        'pandas',
        'python-igraph',
        'tqdm',
        'scipy',
        'joblib',
    ],
    entry_points='''
        [console_scripts]
        tacos=tacos.cli:tacos
    ''',
    test_suite='tests',
)