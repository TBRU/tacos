# TACOS

TACOS is a diverse Set Of Commands And Tools (SOCAT, but TACOS sounds much better!) to handle genomic-derived data.

## Basic setup
Create virtual environment
```
$ conda update conda
$ conda create --prefix=<path/tacos_env> -c intel intelpython3_core python=3.6.* icu=58 pandas scipy numpy joblib matplotlib
$ conda activate <path/tacos_env>
$ conda install -c conda-forge python-igraph tqdm pycairo
$ conda install -c bioconda gffutils
```

Install Tacos:
```
$ conda activate <path/tacos_env> # if not already activated
$ git clone https://git.scicore.unibas.ch/TBRU/tacos.git
$ cd tacos
$ pip install -e .
```

Run the application:
```
$ conda activate <path/tacos_env> # if not already activated
$ tacos --help
```
or
```
$ <path/tacos_env>/bin/python -m tacos --help # no need to activate the environment
```

To run the tests:
```
$ pytest tests
```
