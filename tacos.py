import click
import numpy as np
import pandas as pd
import itertools
from multiprocessing import get_context
import igraph
from functools import partial
import tqdm
def my_hamming(seq1, seq2, missing='X', gap='-', ignore_gap=False, adjust=False):
    """Return the Hamming distance between two given sequence of characters.
    Sequences must have equal length.

    Args:
        seq1: numpy.ndarray (faster) or pandas.Series with sequence of characters.
        seq1: numpy.ndarray (faster) or pandas.Series with sequence of characters.
        missing: Character representing missing value in the sequence.
        adjust: Boolean, default is False for no adjusment accounting for missing values.
    Returns:
        Int, absolute sequence difference after excluding positions with missing values.

    """
    # Raises exception if vectors do not have equal length
    try:
        # get positions excluding missing values
        if ignore_gap:
            seq1_keep = ~(np.in1d(seq1,[missing,gap]))
            seq2_keep = ~(np.in1d(seq2,[missing,gap]))
            #seq1_keep = (seq1 != missing) & (seq1 != gap)
            #seq2_keep = (seq2 != missing) & (seq2 != gap)
        else:
            seq1_keep = (seq1 != missing)
            seq2_keep = (seq2 != missing)
        to_keep = (seq1_keep & seq2_keep)
    except ValueError:
        print("seq1 and seq2 must have equal lengths.")
    else:
        # drop positions with missing values and
        # compute Hamming distance
        # np.sum() instead of sum() improves speed considerably!
        new_seq1 = seq1[to_keep]
        new_seq2 = seq2[to_keep]
        pair_dist = np.sum(new_seq1 != new_seq2)
        # Adjust distance to account for missing values
        if adjust:
            seq1_effective_len = np.sum(seq1_keep)
            seq2_effective_len = np.sum(seq2_keep)
            pair_dist = np.around(len(seq1) * (2 * (pair_dist) / (seq1_effective_len + seq2_effective_len)), decimals=2)

        return (pair_dist)

def get_distance_helper(genome_pairs, data_df, missing='X', gap='-', ignore_gap=False, adjust=False):
    try:
        pair_distances = [(g1, g2, my_hamming(data_df.get(g1).values,
                                              data_df.get(g2).values,
                                              missing, gap, ignore_gap, adjust)) for g1,g2 in genome_pairs]
    except ValueError:
        print("data_df must be a Pandas DataFrame.")
    else:
        return(pair_distances)

@click.group()
def tacos():
    pass

@tacos.command()
def genetic_distance():
    '''Computes the genetic distance between genomes gi and gj, measured by the Hamming distance
    (i.e number of substitutions needed to convert gj into gi)
    '''
    click.echo('compute_distances cmd1')

@tacos.command()
@click.option('-na','--missing', 'missing', default='X', show_default=True, help='Character for missing value')
@click.option('--gap', 'gap', default='-', show_default=True, help='Character to flag a position as indel')
@click.option('--ignore_gap', 'ignore_gap', is_flag=True, help='enable coding gaps(i.e indels) as missing values')
@click.option('--adjust_na', 'adjust', is_flag=True, help='enable adjustment of distance for missing values')
@click.option('--ncores', 'ncores', default=1, show_default=True, help='specify number of processes for parallel computing')
@click.option('--type', default='0', show_default=True, type=click.Choice(['0','1','2']),
              help='either 1 (writes the lower triangle of the matrix), or 0 (writes the upper triangle) or 2 (writes both parts)')
@click.argument('INPUT_', required=True, type=click.Path(exists=True))
@click.argument('OUTPUT', required=True, type=click.Path())
def get_distance_matrix(missing, gap, ignore_gap, adjust, ncores, type, input_, output):
    '''Computes the genetic distance between each genome pair in INPUT. Distances are measured as
    the number of substitutions needed to convert genome g_j to genome g_i (i.e Hamming distance).

    INPUT is the path to the file with variable positions (rows) and genomes (columns)
    '''
    INPUT_file = input_
    positions_genomes = pd.read_csv(INPUT_file, sep='\t', index_col=False)
    nr_genomes = len(positions_genomes.columns[3:])
    genome_pairs = list(itertools.combinations(positions_genomes.columns[3:],2))

    if ncores>1:
        click.echo('Starting pool with {} processes ...'.format(ncores))
        pool = get_context("spawn").Pool(ncores)
        get_distance_partial = partial(get_distance_helper,
                                       data_df=positions_genomes,
                                       missing=missing, gap=gap, ignore_gap=ignore_gap, adjust=adjust)
        genome_pairs_chunks = np.array_split(genome_pairs, ncores)
        distance_mp = tqdm.tqdm(pool.imap_unordered(get_distance_partial, genome_pairs_chunks, chunksize=1),
                                total=len(genome_pairs_chunks))
        pool.close()
        pool.join()
        #with click.progressbar(distance_mp,
        #                       length=len(genome_pairs_chunks),
        #                       label='Computing pairwise distances') as bar:
        #    pair_distances = []
        #    for item in bar:
        #        pair_distances.extend([pair_dist for pair_dist in item])
        pair_distances = [pair_dist for item in distance_mp for pair_dist in item]
        del distance_mp
    else:
        pair_distances = [(g1, g2, my_hamming(positions_genomes.get(g1).values,
                                              positions_genomes.get(g2).values,
                                              missing, gap, ignore_gap, adjust))
                          for g1,g2 in tqdm.tqdm(genome_pairs)]

    del positions_genomes

    genomes_graph=igraph.Graph.TupleList(pair_distances, weights=True)
    del pair_distances

    genomes_names = genomes_graph.vs['name']
    genomes_adj=genomes_graph.get_adjacency(type=np.int(type), attribute='weight')
    del genomes_graph

    with click.open_file(output, 'w') as f:
        f.write('\t'.join(['']+genomes_names))
        f.write('\n')
        for row_ix in range(genomes_adj.shape[0]):
            f.write('\t'.join(map(str,[genomes_names[row_ix]]+genomes_adj[row_ix])))
            f.write('\n')

    click.echo('Finished! Computed {} pairwise distances for {} genomes.'.format(len(genome_pairs), nr_genomes))

if __name__ == '__main__':
    tacos()